var DetailsView = Backbone.View.extend({
	model: Laptop,
	id: "addContainer",
	initialize: function(){
	},
	events:{
		  "click #btnSave": "saveWine",
		  "click #btnDelete": "deleteWine",
		  "click #btnAdd": "add",
	  },
	  saveWine: function(e){
		  alert("model"+this.model.get('make'));
		  var laptopDetails= {make:$('#make').val(), 
				  			model:$('#model').val(),
				  			processor:$('#processor').val(),
				  			price:$('#price').val(),
				  			ram_gb:$('#ram_gb').val() };
		  
		  if (($('#id').val()) == ''){
			  alert("isnew");
			  this.model= new Laptop(laptopDetails);
			  alert("test"+this.model.get('make'));
			  laptopList.add(this.model);
			  alert("added");
			  this.model.save({
					  make:$('#make').val(),  
					  model:$('#model').val(), 
					  processor:$('#processor').val(), 
					  price:$('#price').val(), 
					  ram_gb:$('#ram_gb').val(),     
					  description:$('#description').val()
					  },{
						  success:function(laptop){
							  alert("sucess - added");
							  $('#id').val(laptop.id);
						  }
					  });
			  }
		  else{
		  this.model.save({
			  id:parseInt($('#id').val()),
			  make:$('#make').val(),  
			  model:$('#model').val(), 
			  processor:$('#processor').val(), 
			  price:$('#price').val(), 
			  ram_gb:$('#ram_gb').val(),     
			  description:$('#description').val() 
			  },{
				  success:function(laptop){
					  alert("sucess - updated");
					  mainView.renderList();
					  
				  }
			  });
		  }
		  return false;
	  },
	  deleteWine: function(e){
		  var laptopId= parseInt($('#id').val());
		  this.model.set({id:laptopId});
		  this.model.destroy(
				  {success:function(data){
					  alert("laptop deleted");
					  mainView.renderList();
				  }
		  });
		  return false;
	  },
	  
	  add: function(e){
		  $('#btnDelete').hide();
		  alert('Add');
		  $('#id').val(null);
		  $('#make').val("");  
		  $('#model').val("");
		  $('#processor').val("");
		  $('#price').val(""); 
		  $('#ram_gb').val("");     
		  $('#description').val("") ;
		  return false;
	  },
	
  render: function(){
	  console.log("render"+this.model.get('model'));
	  //need to change this line for versions of underscore >= 1.7
	 var template= _.template($('#wine-details').html(), this.model.toJSON());
	 return this.$el.html(template);
  }
});

