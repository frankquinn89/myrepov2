var rootUrl = "http://localhost:8080/FranksLaptopApp/rest/laptops/";
var mainView;
var laptopList;
var getLaptopById;
var t;
var laptopId;
var renderTable= function(data) {
	t.clear();
	$.each(data, function(index, laptop) {	
		t.row.add([laptop.id, laptop.make,laptop.model, laptop.processor, laptop.ram_gb, laptop.price ,'<button><i style="font-size:24px" class="fa">&#xf055;</i></button>']).draw( false );
	});
}

var getAll = function() {
	$.ajax({
		type : "GET",
		url : rootUrl,
		dataType : "json",
		success : renderTable
	});
}

//0-----------------------------------------------------
$(document).ready(function(){
	t = $('#example').DataTable();
	getAll();
	
	$('#example tbody').on('click', 'tr', function(){
		var id = $(this).find('td');
		getLaptopById = id[0].innerHTML;
		$("#myModal").modal(); 
	});
	
	$('#close, #hiddenbtn').on('click', function(){
		console.log("here");
		laptopList.fetch({
			success: function(data){
				t.clear().draw();
				renderTable(data.toJSON());
			}
		});
	});
	
	laptopList = new LaptopList();
	laptopList.fetch({
		success: function(data){
			mainView = new MainView({collection: laptopList});
			mainView.$el.appendTo(document.body);
		}
	});
});