//WineView builds a list item for each wine.
var LaptopView = Backbone.View.extend({
  model: Laptop,
  tagName:'li',
  events:{
	  "click li": "alertStatus",
		  "click close": "close"
  },
  alertStatus: function(e){
	  var laptop=this.model;
	  $('#mainArea').html(new DetailsView({model:laptop}).render());
  },
    render:function(){
		 var template= _.template($('#wine_list').html(), this.model.toJSON());
		return this.$el.html(template);
  }
  , close:function(){
	  $('#myModal').modal('close');
	  $('#hiddenbtn').trigger('click');
  }
});