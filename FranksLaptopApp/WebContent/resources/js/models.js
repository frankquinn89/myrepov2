
var rootURL = "http://localhost:8080/FranksLaptopApp/rest/laptops";

var Laptop = Backbone.Model.extend({
	urlRoot:rootURL,
	defaults:{  
		"id":null, 
		"make":"",     
		"model":"",  
		"processor":"", 
		"price":"",  
		"ram_gb":"",      
		"description":"",  
		"image":""    },
  initialize: function(){
    console.log("wine init");
    //this.on('change', function(){
    //	console.log('Values for a model have changed');
    //});
  }
});

var LaptopList = Backbone.Collection.extend({
	model: Laptop,
	url:rootURL
	});
