//MainView grabs all the rendered subviews (WineViews) and appends them.
var MainView = Backbone.View.extend({
  collection: LaptopList,
  id: "container",
  initialize: function(){
   this.listenTo(this.collection, 'add', this.renderList);
   this.render();
  },

  render: function(){
    this.collection.each(function(laptop){
    $('#wineList').append(new LaptopView({model:laptop}).render());
    }, this);
    var laptop = new Laptop(); 
    $('#mainArea').html(new DetailsView({model:laptop}).render());
  },
  
  renderList:function(){
	  $('#wineList li').remove();
	  this.collection.each(function(laptop){
		    $('#wineList').append(new LaptopView({model:laptop}).render());
		  //  }, this);
  });
 
  }
});

