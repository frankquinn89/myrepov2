
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.core.Response;

import org.apache.commons.httpclient.HttpStatus;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.example.rest.Laptop;
import com.example.rest.LaptopDAO;
import com.example.rest.LaptopWS;
import com.example.rest.RestApplication;



	//	@FixMethodOrder(MethodSorters.NAME_ASCENDING)
		@RunWith(Arquillian.class)
		public class WSTest {
			
			@Deployment
			public static Archive<?> createTestArchive() {
				return ShrinkWrap
						.create(JavaArchive.class, "Test.jar")
						.addClasses(LaptopDAO.class, Laptop.class,
								RestApplication.class,LaptopWS.class)
						.addAsManifestResource("META-INF/persistence.xml",
								"persistence.xml")
						.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

			}

			@EJB
			private LaptopWS laptopWS;
			
			@EJB
			private LaptopDAO laptopDAO;
		
		
			
			@Test
			public void testGetByIDResponse() {
					Response response=laptopWS.getLaptopByID(1);
					Laptop laptop = (Laptop) response.getEntity();
					assertEquals(laptop.getMake(),"Asus");
					assertEquals(laptop.getImage(), "asus1.jpg");
					assertEquals(laptop.getModel(), "MA-190");
				}
			
			@Test
			public void testGetByMake() {
					Laptop laptop = new Laptop();
					laptop.setModel("AlienWare");
					Response response=laptopWS.findAllMakes("AlienWare");
					List<Laptop> laptops = (List<Laptop>) response.getEntity();
					assertEquals(laptops.size(), 0);
				}
			
			@Test
			public void testCRUD() {
				//Create
					Laptop laptop = new Laptop();
					laptop.setMake("make");
					Response response=laptopWS.saveLaptop(laptop);
					assertEquals(HttpStatus.SC_CREATED, response.getStatus());
					//Update
					//laptop.setMake("new Make");
					response = laptopWS.updateLaptop(laptop);
					assertEquals(HttpStatus.SC_OK, response.getStatus());
					
					//Read
					response = laptopWS.findAll();
					assertEquals(HttpStatus.SC_OK, response.getStatus());
					//Delete
					response = laptopWS.deleteLaptop(laptop.getId());
					assertEquals(HttpStatus.SC_NO_CONTENT, response.getStatus());
					//laptopDAO.delete(laptop.getId());
				}
				
		}