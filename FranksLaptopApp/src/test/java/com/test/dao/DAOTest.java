package com.test.dao;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.example.rest.Laptop;
import com.example.rest.LaptopDAO;
import com.example.rest.LaptopWS;
import com.example.rest.RestApplication;

		@RunWith(Arquillian.class)
		public class DAOTest {
			
			@Deployment
			public static Archive<?> createTestArchive() {
				return ShrinkWrap
						.create(JavaArchive.class, "Test.jar")
						.addClasses(LaptopDAO.class, Laptop.class,
								RestApplication.class,LaptopWS.class)
						.addAsManifestResource("META-INF/persistence.xml",
								"persistence.xml")
						.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

			}

			 
			@EJB
			private LaptopWS laptopWS;
			
			@EJB
			private LaptopDAO laptopDAO;
			
			
			@Test
			public void testGetAllLaptops() {
				List<Laptop> laptopList = laptopDAO.getAllLaptops();
				assertEquals("Data fetch = data persisted", laptopList.size(), 11);
			}
			
			@Test
			public void testGetByMake() {
				List<Laptop> laptopList = laptopDAO.getLaptopsByMake("asus");
				assertEquals("Data fetch = data persisted", laptopList.size(), 3);
			}
			
			@Test
			public void testGetByPrice() {
				List<Laptop> laptopList = laptopDAO.getLaptopsByPrice(0);
				assertEquals("Data fetch = data persisted", laptopList.size(), 0);
			}

			
			@Test
			public void testGetById() {
				Laptop laptop = laptopDAO.getLaptop(1);
				assertEquals(laptop.getId(),1);
			}
			
			@Test
			public void testAddUpdateDelete() {
				Laptop laptop = new Laptop();
				laptopDAO.save(laptop);
				laptop.setMake("y"); 
				laptopDAO.update(laptop);
				assertEquals(laptop.getMake(), "y");
				laptopDAO.delete(laptop.getId());
			}
					
			
			
}
