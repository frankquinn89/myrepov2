package com.test.selenium;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import javax.ejb.Timeout;

import org.apache.poi.hssf.record.formula.functions.Find;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.omg.CORBA.TIMEOUT;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumTests {
	WebDriver driver;

	@Before
	public void setUp(){
		System.setProperty("webdriver.gecko.driver", "C:\\geckodriver\\geckodriver.exe");
		driver = new FirefoxDriver();
	driver.get("http:\\localhost:8080\\FranksLaptopApp");
	}
	
	@Test
	public void testSeleniumGetTitle() {
		assertEquals("FranksLaptopApp", driver.getTitle());
	}

	@Test
	public void testSeleniumClickTabs() throws InterruptedException  {
		WebElement tab = driver.findElement(By.id("home"));
		TimeUnit.SECONDS.sleep(2);
		tab.click();
		tab = driver.findElement(By.id("dataTable"));
		TimeUnit.SECONDS.sleep(2);
		tab.click();
		tab = driver.findElement(By.id("CRUD"));
		TimeUnit.SECONDS.sleep(2);
		tab.click();
	}
	
	@Test
	public void testAddLaptop() throws InterruptedException  {
		WebElement tab = driver.findElement(By.id("CRUD"));
		TimeUnit.SECONDS.sleep(2);
		tab.click();
		driver.findElement(By.id("make")).sendKeys("TEST-MAKE");
		driver.findElement(By.id("model")).sendKeys("TEST-MODEL");
		WebElement saveButton = driver.findElement(By.id("save"));
		TimeUnit.SECONDS.sleep(2);
		saveButton.click();
	}


	
	@After
	public void tearDown() throws Exception{
		driver.quit();
	}

}

