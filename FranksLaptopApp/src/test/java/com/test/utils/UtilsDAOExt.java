package com.test.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class UtilsDAOExt {

	Connection connection;

	private Connection getConnection() {
		Connection conn = null;
		String url = "jdbc:mysql://localhost/testdatabase?user=root&&password=admin";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	public void deleteTable() {
		Connection conn = getConnection();
		try {
			Statement statement = conn.createStatement();
			String sql = "DROP TABLE IF EXISTS laptops";
			statement.executeUpdate(sql);
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addTwoRows() {
		Connection conn = getConnection();
		Statement stmt;
		PreparedStatement ps = null;
		try {
			String sql = "CREATE TABLE laptops "
					+ "(laptopId int(11) NOT NULL AUTO_INCREMENT, "
					+ " make varchar(255) DEFAULT NULL,"
					+ " model varchar(255) DEFAULT NULL,"
					+ " processor varchar(255) DEFAULT NULL, "
					+ " ram_gb int(11) DEFAULT NULL, "
					+ " storage_gb int(11) DEFAULT NULL, "
					+ " price double DEFAULT NULL, "
					+ " description varchar(255) DEFAULT NULL, "
					+ " image varchar(255) DEFAULT NULL, "
					+ " PRIMARY KEY (laptopId)) ";
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			ps = conn
					.prepareStatement(
							"INSERT INTO laptops (laptopId, make, model, processor, ram_gb, storage_gb, price, description, image) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
							new String[] { "laptopId" });
			ps.setString(1, "asus");
			ps.setString(2, "x123");
			ps.setString(3, "amd");
			ps.setInt(4, 8);
			ps.setInt(5, 1000);
			ps.setDouble(6, 500);
			ps.setString(
					7,
					"excellent laptop");
			ps.setString(8, "asus1.jpg");
			ps.executeUpdate();
//			ps = conn
//					.prepareStatement(
//							"INSERT INTO laptop (make, model, processor, ram_gb, storage_gb, price, description, image) VALUES (?, ?, ?, ?, ?, ?, ?)",
//							new String[] { "ID" });
//			ps.setString(1, "apple");
//			ps.setString(2, "ipad");
//			ps.setString(3, "i3");
//			ps.setInt(4, 6);
//			ps.setInt(5, 500);
//			ps.setDouble(6, 600);
//			ps.setString(
//					7,
//					"good laptop");
//			ps.setString(8, "apple.jpg");
//			ps.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
