package com.test.apache;
import static org.junit.Assert.*;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Assert;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;

import com.example.rest.Laptop;
import com.google.gson.Gson;
import com.test.utils.RestApiHelper;
import com.test.utils.RestResponse;
import com.test.utils.UtilsDAOExt;



public class RestAPITest {


	@Test
	public void testRestApiGetAll() {
		String url = "http://localhost:8080/FranksLaptopApp/rest/laptops/";
		RestResponse response = RestApiHelper.performGetRequest(url, null);
		assertEquals(HttpStatus.SC_OK, response.getStatusCode());
		Laptop[] laptopList = new Gson().fromJson(response.getResponseBody(),
				Laptop[].class);
		assertEquals(11, laptopList.length);
	
	}

	@Test
	public void testRestApiGetById() {
		String url = "http://localhost:8080/FranksLaptopApp/rest/laptops/1";
		RestResponse response = RestApiHelper.performGetRequest(url, null);
		Assert.assertEquals(HttpStatus.SC_OK, response.getStatusCode());
		Laptop laptop = new Gson().fromJson(response.getResponseBody(), Laptop.class);
		Assert.assertEquals("Asus",laptop.getMake());
		Assert.assertEquals("MA-190", laptop.getModel());
		Assert.assertEquals(540, laptop.getPrice(),0);
	}
	@Test
	public void testPostLapotp() {
		String fileName = "TestDataFile";
		Map<String, String> headers = new LinkedHashMap<String, String>();
		headers.put("Accept", "application/json");
	headers.put("Content-Type", "application/json");
		RestResponse response = RestApiHelper.performPostRequest(
				"http://localhost:8080/FranksLaptopApp/rest/laptops/", fileName,
				headers);
		Assert.assertEquals(HttpStatus.SC_CREATED, response.getStatusCode());
		Laptop laptop = new Gson().fromJson(response.getResponseBody(), Laptop.class);
		String url = "http://localhost:8080/FranksLaptopApp/rest/laptops/"
				+ laptop.getId();
		response = RestApiHelper.performGetRequest(url, null);
		Assert.assertEquals(HttpStatus.SC_OK, response.getStatusCode());
		Assert.assertEquals("Asus", laptop.getMake());
		Assert.assertEquals("MA-190", laptop.getModel());
		Assert.assertEquals("i3", laptop.getProcessor());
	}
	
	@Test
	public void testDeleteLaptop() {
		String url = "http://localhost:8080/FranksLaptopApp/rest/laptops/1";
		RestResponse response = RestApiHelper.performDeleteRequest(url);
		Assert.assertEquals(HttpStatus.SC_NO_CONTENT, response.getStatusCode());
		response = RestApiHelper.performGetRequest(url, null);
		Assert.assertEquals(HttpStatus.SC_OK, response.getStatusCode());
		Laptop laptop = new Gson().fromJson(response.getResponseBody(), Laptop.class);
		Assert.assertEquals(null, laptop);
		url = "http://localhost:8080/FranksLaptopApp/rest/laptops/";
		response = RestApiHelper.performGetRequest(url, null);
		assertEquals(HttpStatus.SC_OK, response.getStatusCode());
		Laptop[] laptopList = new Gson().fromJson(response.getResponseBody(),
				Laptop[].class);
		assertEquals(1, laptopList.length);
	}
}