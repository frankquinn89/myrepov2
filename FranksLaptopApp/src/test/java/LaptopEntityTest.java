
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.example.rest.Laptop;
import com.example.rest.LaptopDAO;
import com.example.rest.LaptopWS;
import com.example.rest.RestApplication;



	//	@FixMethodOrder(MethodSorters.NAME_ASCENDING)
		@RunWith(Arquillian.class)
		public class LaptopEntityTest {
			
			@Deployment
			public static Archive<?> createTestArchive() {
				return ShrinkWrap
						.create(JavaArchive.class, "Test.jar")
						.addClasses(LaptopDAO.class, Laptop.class,
								RestApplication.class,LaptopWS.class)
					//	.addPackage(EventCause.class.getPackage())
					//	.addPackage(EventCauseDAO.class.getPackage())
								//this line will pick up the production db
						.addAsManifestResource("META-INF/persistence.xml",
								"persistence.xml")
						.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

			}


			
			@Test
			public void testGettersSetters() {
				Laptop laptop = new Laptop();
				laptop.setId(1);
				laptop.setDescription("desc");
				laptop.setImage("image");
				laptop.setMake("make");
				laptop.setModel("model");
				laptop.setPrice(1);
				laptop.setProcessor("proc");
				laptop.setRam_gb(2);
				laptop.setStorage_gb(3);
				assertEquals(laptop.getId(),1);
				assertEquals(laptop.getDescription(),"desc");
				assertEquals(laptop.getImage(),"image");
				assertEquals(laptop.getMake(),"make");
				assertEquals(laptop.getModel(),"model");
				assertEquals(laptop.getPrice(),1,0);
				assertEquals(laptop.getProcessor(),"proc");
				assertEquals(laptop.getRam_gb(),2);
				assertEquals(laptop.getStorage_gb(), 3);
			}
			
			
			
}
