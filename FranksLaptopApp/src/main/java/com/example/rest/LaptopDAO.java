package com.example.rest;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;



@Stateless
@LocalBean
public class LaptopDAO {
 
    @PersistenceContext
    private EntityManager entityManager;
    
    public Laptop getLaptop(final int laptopId) {
        return entityManager.find(Laptop.class, laptopId);
    }
    
    
    
//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
//    public void addLaptops(final List<Laptop> laptops) {
//        for (final Laptop laptop : laptops) {
//            entityManager.persist(laptop);
//        }
//    }
    
    public List<Laptop> getAllLaptops(){
		 final Query query = entityManager.createQuery("SELECT w FROM Laptop w");
		return query.getResultList();
		
	}
    
    
    public List<Laptop> getLaptopsByMake(final String make){
		final Query query = entityManager.createQuery("SELECT w FROM Laptop w WHERE w.make LIKE"+ "'%" + make + "%'");
		return query.getResultList();
		
	}
    public List<Laptop> getLaptopsByPrice(final int price) {
    	final Query query = entityManager.createQuery("SELECT w FROM Laptop w WHERE w.price <="+ price);
    	return query.getResultList();
	}
    
//    public List<Laptop> getLaptopsByRAM(final int ram_gb) {
//    	final Query query = entityManager.createQuery("SELECT w FROM Laptop w WHERE w.ram_gb >="+ ram_gb);
//		return query.getResultList();
//	}
    
    public void save(final Laptop laptop){
    	entityManager.persist(laptop);
    }
    
    public void update(final Laptop laptop){
    	entityManager.merge(laptop);
    }
    
    public void delete(final int laptopId){
    	entityManager.remove(getLaptop(laptopId));
    }
    
}
