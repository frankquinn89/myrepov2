package com.example.rest;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Laptop {
    
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int laptopId;
 
    private String make;
    
    private String model;
    
    private String processor;
    
    private int ram_gb;

	private int storage_gb;
    
    private double price;
    
    private String description;
    
    private String image;
    
    
    
    public String getMake() {
		return make;
	}

	public void setMake(final String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(final String model) {
		this.model = model;
	}

	public String getProcessor() {
		return processor;
	}

	public void setProcessor(final String processor) {
		this.processor = processor;
	}

	public int getRam_gb() {
		return ram_gb;
	}

	public void setRam_gb(final int ram_gb) {
		this.ram_gb = ram_gb;
	}

	public int getStorage_gb() {
		return storage_gb;
	}

	public void setStorage_gb(final int storage_gb) {
		this.storage_gb = storage_gb;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(final double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

    public int getId() {
        return laptopId;
    }

    public void setId(final int laptopId) {
        this.laptopId = laptopId;
    }

    public void setImage(final String image){
    	this.image=image;
    }
    
    public String getImage(){
    	return image;
    }

}
