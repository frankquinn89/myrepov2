package com.example.rest;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/laptops")
@Stateless
@LocalBean
public class LaptopWS {

	@EJB
	private LaptopDAO laptopDAO;

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getLaptopByID(@PathParam("id") final int laptopId) {
		final Laptop laptop = laptopDAO.getLaptop(laptopId);
		return Response.status(200).entity(laptop).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAll() {
		final List<Laptop> laptops = laptopDAO.getAllLaptops();
		return Response.status(200).entity(laptops).build();
	}

	@GET
	@Path("make/{make}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAllMakes(@PathParam("make") final String make) {
		final List<Laptop> makes = laptopDAO.getLaptopsByMake(make);
		return Response.status(200).entity(makes).build();
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	public Response saveLaptop(final Laptop laptop) {
		laptopDAO.save(laptop);
		return Response.status(201).entity(laptop).build();
	}

	@PUT
	@Path("/{id}")
	@Consumes({ "application/json" })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateLaptop(final Laptop laptop) {
		laptopDAO.update(laptop);
		return Response.status(200).entity(laptop).build();
	}

	@DELETE
	@Path("/{id}")
	public Response deleteLaptop(@PathParam("id") final int laptopId) {
		laptopDAO.delete(laptopId);
		return Response.status(204).build();
	}

}